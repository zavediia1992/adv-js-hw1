
class Employee {
    constructor(name,age, salary) {
        this._name = name;
        this._age = age;
        this._salary = salary;
    }
    set name(newName) {
        this._name = newName;
    }
    set age(newAge) {
        this._age = newAge;
    }
    set salary(newSalary) {
        this._salary = newSalary;
    }

    get name() {
        return this._name;
    }
    get age() {
        return this._age;
    }
    get salary() {
        return this._salary;
    }

}

class Programmer extends Employee {
    constructor(name,age,salary,lang) {
        super(name,age,salary);
        this._lang = lang;
    }
    get get_salary() {
        return this._salary * 3;
    }
}

let pr1 = new Programmer("vasa", 20, 300, "english");
let pr2 = new Programmer("asa", 2330, 30440, "japan");


console.log(pr1.get_salary);
console.log(pr2.get_salary);

console.log(pr1);
console.log(pr2);